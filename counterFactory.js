let counterFactory = function () {
   let counter = 0;
   function increment() {

      counter = counter + 1

      return counter;
   }


   function decrement() {
      counter = counter - 1
      return counter;
   }
   return { increment, decrement }



}

module.exports = counterFactory;