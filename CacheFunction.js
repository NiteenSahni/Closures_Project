let cacheFunction = function (cb) {
    let cache = {};
    function cbInvoke() {

        if (cache.hasOwnProperty([...arguments])) {
            return cache[[...arguments]]

        }
        else {
            cache[[...arguments]] = cb(...arguments)
            return ("cached")
        }



    }
    return cbInvoke


}

module.exports = cacheFunction


