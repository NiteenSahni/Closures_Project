let limitFunctionCallCount = function (cb, n) {
    let count = 1;

    let cbTracker = function () {
        if (count <= n) {
            count = count + 1

            let OutcomeCB = cb();
            if (OutcomeCB == undefined) {
                return null
            }
            else {
                return OutcomeCB;
            }
        }
        if (count > n) {
            return "Count Exceeded"
        }
    }

    return cbTracker
}

module.exports = limitFunctionCallCount;
